﻿using System;
using System.IO;

namespace Hydac_Check_in_System___Team_2A
{
    class Program
    {
        static void Main(string[] args)
        {
            Room.RemoveOldBookings();
            Guest.RemoveOldCheckIns();
            Employee.RemoveOldCheckIns();

            Menu mainMenu = new Menu("HYDAC Menu");

            mainMenu.AddMenuItem("1. Check ind");
            mainMenu.AddMenuItem("2. Check ud");
            mainMenu.AddMenuItem("3. Book et lokale");
            mainMenu.AddMenuItem("4. Vis hvem der er checket ind");
            mainMenu.AddMenuItem("5. Afslut\n");


            startOver:
            mainMenu.Show();
            retryInput:
            string userInput = Console.ReadLine();
            switch (userInput)
            {
                case "1":
                    {
                        Console.Clear();
                        bool inputValid = false;
                        while (inputValid != true)
                        {
                            Console.WriteLine("1. Medarbejder check-in \n2. Gæste check-in\n");
                            userInput = Console.ReadLine();
                            Console.Clear();
                            if (userInput == "1")
                            {
                                Employee employee = new Employee();
                                employee.CheckIn();
                                inputValid = true;
                            }
                            else if (userInput == "2")
                            {
                                Guest guest = new Guest();
                                guest.CheckIn();
                                inputValid = true;
                            }
                            else
                            {
                                Console.WriteLine("Ugyldigt input. Prøv igen");
                            }
                        }

                        Console.Clear();
                        goto startOver;

                    }
                case "2":
                    {
                        Console.Clear();
                        Console.WriteLine("1. Medarbejder check-ud \n2. Gæste check-ud\n");
                        userInput = Console.ReadLine();
                        if (userInput == "1")
                        {
                            Employee employee = new Employee();
                            employee.CheckOut();
                        }
                        else if (userInput == "2")
                        {
                            Guest guest = new Guest();
                            guest.CheckOut();
                        }
                        Console.Clear();
                        goto startOver;
                    }
                case "3":
                    {
                        Console.Clear();
                        Console.WriteLine("1. Book et rum \n2. Se bookede rum\n");
                        userInput = Console.ReadLine();
                        if (userInput == "1")
                        {
                            Console.Clear();
                            Console.WriteLine("Vælg det rum du ønsker at booke\n");
                            Room.DisplayRooms();
                            Console.WriteLine("\n");
                            bool success = int.TryParse(Console.ReadLine(), out int selection);
                            Console.Clear();
                            if (success)
                            {
                                Room room = new Room();
                                room.RoomSelection(selection);
                            }
                            else
                            {
                                Console.WriteLine("Ugyldigt input, prøv igen");
                            }
                        }
                        else if (userInput == "2")
                        {
                            Room show = new Room();
                            show.ShowBookedRooms();
                        }

                        Console.Clear();
                        goto startOver;
                    }
                case "4":
                    {
                        Console.Clear();
                        Employee employee = new Employee();
                        employee.ShowCheckedIn();
                        Guest guest = new Guest();
                        guest.ShowCheckedIn();
                        Console.Write("\nTryk Enter for at forsætte");
                        Console.ReadLine();
                        Console.Clear();
                        goto startOver;
                    }
                case "5":
                    {
                        Console.Clear();
                        Console.WriteLine("Er du sikker? ja/nej");

                        bool validInput = false;
                        while(validInput == false)
                        {
                            string input = Console.ReadLine();
                            if (input == "ja")
                            {
                                Console.Clear();
                                Console.WriteLine("Farvel");
                                validInput = true;
                            }
                            else if (input == "nej")
                            {
                                validInput = true;
                                Console.Clear();
                                goto startOver;
                            }
                            else
                            {
                                Console.WriteLine("Tast enten \"ja\" eller \"nej\"");
                            }

                        }
                        break;
                    }

                default:
                    Console.Clear();
                    Console.WriteLine("Ugyldigt input. Prøv igen.\n");
                    goto retryInput;
            }

        }
    }
}
