﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Hydac_Check_in_System___Team_2A
{
    public class Employee
    {
        private int employeeId;
        public int EmployeeId
        {
            get { return employeeId; }
        }
        private string name;
        public string Name
        {
            get { return name; }
        }
        private string team;

        public string Mood { get; set; }
        
   

        public void CheckIn()
        {
            Console.Clear();
            IdInput();
            SelectMood();

            StreamWriter writer = new StreamWriter("employeeCheckIns\\" + employeeId + " " + DateTime.Now.ToString("dd-MM-yyyy")  +  ".txt");
            writer.WriteLine(employeeId + ";" + name + ";" + team + ";" + DateTime.Now.ToString("dd-MM-yyyy HH:mm") + ";" + Mood);
            writer.Close();
            Console.Clear();
            Console.WriteLine("Du er nu checket ind som:\n\nNavn: " + name + "\nMedarbejderID: " + employeeId + "\nTeam: " + team + "\nTidspunkt for check-in: " + DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "\nHumør: " + Mood + "\n\nTryk enter for at fortsætte");
            Console.ReadLine();
        }

        public void CheckOut()
        {
            IdInput();
            string[] listOfEmployeeCheckIns = Directory.GetFiles("employeeCheckIns\\");
            foreach (var path in listOfEmployeeCheckIns)
            {
                StreamReader sr = new StreamReader(path);
                string employeeCheckIn = sr.ReadLine();
                sr.Close();
                string[] employeeCheckInSubStrings = employeeCheckIn.Split(';');
                string checkInId = employeeCheckInSubStrings[0];
                if (checkInId.Contains(employeeId.ToString()))
                {
                    File.Delete(path);
                    Console.WriteLine("Du er hermed checket ud. Tryk enter for at forsætte");
                    Console.ReadLine();
                    Console.Clear();
                }


            }
        }

        public void ShowCheckedIn()
        {
            string[] listOfEmployeeCheckIns = Directory.GetFiles("employeeCheckIns\\");
            Console.WriteLine("Medarbejdere\n");
            foreach (var path in listOfEmployeeCheckIns)
            {
                StreamReader sr = new StreamReader(path);
                string employeeCheckin = sr.ReadLine();
                sr.Close();
                string[] employeeCheckInSubStrings = employeeCheckin.Split(';');
                Console.WriteLine("Navn: " + employeeCheckInSubStrings[1] + "\nHumør: " + employeeCheckInSubStrings[4] + "\nTeam: " + employeeCheckInSubStrings[2] + "\nTidspunkt for check-in: " + employeeCheckInSubStrings[3] + "\n");
            }

 
        }

        public void IdInput()
        {
            Console.WriteLine("Indtast venligst dit medarbejder ID. (1, 2 og 3 er gyldige ID'er)\n");
            bool idIsFound = false;


            //System gets all employee files, and saves their path in an array
            string[] listOfEmployees = Directory.GetFiles("employees\\");

            while (idIsFound == false)
            {
                try
                {
                    employeeId = int.Parse(Console.ReadLine());         //User enters employee ID
                }
                catch (Exception)
                {
                    employeeId = 0;
                }


                for (int i = 0; i < listOfEmployees.Length; i++)
                {

                    string path = listOfEmployees[i];                   //Each path is put in a variable
                    string[] pathSubStrings = path.Split('\\', '.');    //we split the string into multiple strings, and is put in an array. We do this so we can isolate the filename
                    string fileName = pathSubStrings[1];

                    if (int.Parse(fileName) == employeeId)              //Checks if the employee ID matches a filename. The files names are the employee IDs exactly.
                    {
                        StreamReader reader = new StreamReader(path);   //When a match is found we read the contents of the file.
                        string fileContents = reader.ReadLine();
                        reader.Close();
                        string[] employeeSubStrings = fileContents.Split(';');
                        name = employeeSubStrings[1];
                        team = employeeSubStrings[2];
                        Mood = employeeSubStrings[3];


                        idIsFound = true;
                    }
                }
                if (idIsFound == false)
                {
                    Console.WriteLine("\nDet indtastede medarbejder ID eksisterer ikke. Prøv igen.\n");
                }
            }


            
        }

        public void SelectMood()
        {
            Console.Clear();
            Console.WriteLine("Hvad er dit humør?\n");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1. :-)");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("2. :-|");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("3. :-(");
            Console.ResetColor();
            Console.WriteLine("4. Brugerdefineret humør\n");
            bool inputValid = false;
            int myMood = 0;
            while (inputValid != true)
            {
                try
                {
                    myMood = Convert.ToInt32(Console.ReadLine());
                    inputValid = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            switch (myMood)
            {
                case 1:
                    {
                        Mood = ":-)";
                        break;
                    }
                case 2:
                    {
                        Mood = ":-|";
                        break;
                    }
                case 3:
                    {
                        Mood = ":-(";
                        break;
                    }
                case 4:
                    {
                        Console.Clear();
                        Console.WriteLine("Hvilket humør er du i?");
                        Mood = Console.ReadLine();
                        break;
                    }
                default:
                    {
                        Console.WriteLine("\nUgyldigt input, prøv igen.\n");
                        Console.ReadLine();
                        break;
                    }
            }

        }

        public static void RemoveOldCheckIns()
        {
            string[] bookedRooms = Directory.GetFiles("employeeCheckIns\\");
            foreach (var path in bookedRooms)
            {
                StreamReader sr = new StreamReader(path);
                string[] checkInSubStrings = sr.ReadLine().Split(';');
                DateTime checkInTime = DateTime.Parse(checkInSubStrings[3]);
                sr.Close();
                int dateTimeComparison = DateTime.Compare(checkInTime, DateTime.Today);
                if (dateTimeComparison == -1)
                {
                    File.Delete(path);
                }
            }
        }
    }
}
