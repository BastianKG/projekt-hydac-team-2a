﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Hydac_Check_in_System___Team_2A
{
    public class Room
    {
        public string Path { get; set; }
        public string RoomId { get; set; }

        public static void DisplayRooms()
        {
            //Print alle lokale filer fra mappen room
            string[] filesArray = Directory.GetFiles("room\\");
            for (int i = 0; i < filesArray.Length; i++)
            {
                string[] textFileName = filesArray[i].Split('\\', '.');
                string roomName = textFileName[1];
                Console.WriteLine(i + 1 + ". " + roomName);
            }
        }

        public void RoomSelection(int selection)
        {
            string[] rooms = Directory.GetFiles("room\\");
            Path = rooms[selection-1];

            string[] textFileName = rooms[selection-1].Split('\\', '.');
            RoomId = textFileName[1];
            AddBooking();
        }
       
        public void AddBooking()
        {
            //Opretter forbindelse til tekstfilen, ud fra objektets Path property
            //Bruger angiver start og slut tid, samt navn
            StreamWriter sw = new StreamWriter(Path);
            Console.WriteLine("Angiv booking starttidspunkt: (år-måned-dag) (time:minut)\n");
            bool tryAgain = true;
            while (tryAgain)
            {
                try
                {
                    string bookStart = Console.ReadLine();
                    DateTime bookStartDate = DateTime.Parse(bookStart);
                    sw.WriteLine("Booket fra: " + bookStartDate);
                    tryAgain = false;
                }
                catch (Exception)
                {
                    Console.WriteLine("\nUgyldig input, prøv igen.\n");
                }
            }
            Console.WriteLine("\nAngiv booking sluttidspunkt: (år-måned-dag) (time:minut)\n");
            tryAgain = true;
            while (tryAgain)
            {
                try
                {
                    string bookEnd = Console.ReadLine();
                    DateTime bookEndDate = DateTime.Parse(bookEnd);
                    sw.WriteLine("Booket til: " + bookEndDate);
                    tryAgain = false;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            Console.Clear();
            Console.WriteLine("Indtast navnet på personen som booker dette rum");
            string personResponsible = Console.ReadLine();
            sw.WriteLine("Booket af : " + personResponsible);
            sw.Close();
            //Flytter filen til roomsBooked mappen, hvis det ikke er muligt, er rummet allerede booket.
            try
            {
                File.Move(Path, "roomsBooked\\" + RoomId + " (Booked)" + ".txt");
            }
            catch
            {
                Console.WriteLine("\nFEJL (Rummet er allerede booket, vælg venligst et andet)");
                Console.ReadLine();
            }
            //Gendanner en tom lokale fil
            StreamWriter refreshFile = new StreamWriter(Path);
            refreshFile.Close();

        }

        public void ShowBookedRooms()
        {
            Console.Clear();
            string[] filesArray = Directory.GetFiles("roomsBooked\\");
            if (filesArray.Length == 0)
            {
                Console.WriteLine("Der er ingen rum booket lige nu");
            }
            for (int i = 0; i < filesArray.Length; i++)
            {
                string[] textFileName = filesArray[i].Split('\\', '.');
                Console.WriteLine(textFileName[1]);
                StreamReader sr = new StreamReader(filesArray[i]);
                string allLines = sr.ReadToEnd();
                Console.WriteLine(allLines);
                sr.Close();
            }
            Console.WriteLine("Tryk enter for at vende tilbage til hovedmenuen");
            Console.ReadLine();
        }

        public static void RemoveOldBookings()
        {
            string[] bookedRooms = Directory.GetFiles("roomsBooked\\");
            foreach (var path in bookedRooms)
            {
                StreamReader sr = new StreamReader(path);
                sr.ReadLine();
                DateTime bookingEndTime = DateTime.Parse(sr.ReadLine().Remove(0, 12));
                sr.Close();
                int dateTimeComparison = DateTime.Compare(bookingEndTime, DateTime.Today);
                if(dateTimeComparison == -1)
                {
                    File.Delete(path);
                }
            }
        }
    }
}
