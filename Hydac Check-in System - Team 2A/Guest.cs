﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Hydac_Check_in_System___Team_2A
{
    class Guest
    {
        private string name;
        private string company;
        private string responsiblePerson;
        private DateTime dateOfArrival;
        private string safetyBrochuresDelivered;

        public void CheckIn()
        {
            Console.WriteLine("Indtast venligst gæstens navn\n");
            name = Console.ReadLine();

            Console.Clear();
            Console.WriteLine("Indtast venligst firma\n");
            company = Console.ReadLine();

            Console.Clear();
            Console.WriteLine("Indtast venligst navnet på medarbejderen som er ansvarlig for gæsten\n");
            responsiblePerson = Console.ReadLine();

            SafetyBrochure sb = new SafetyBrochure();
            safetyBrochuresDelivered = sb.RetrieveSafetyBrochures();

            string temp = safetyBrochuresDelivered;
            safetyBrochuresDelivered = "";
            string isSafetyBrochureDeliveredString;
            if (temp.Contains("None delivered"))
            {
                isSafetyBrochureDeliveredString = "Nej";
                safetyBrochuresDelivered = "Ingen sikkerhedsbrochurer afleveret";
            }
            else
            {
                isSafetyBrochureDeliveredString = "Ja";
                string[] safetyBrochuresDeliveredSubStrings = temp.Split(';');
                foreach (var safetyBrochure in safetyBrochuresDeliveredSubStrings)
                {
                    safetyBrochuresDelivered += safetyBrochure + " og ";
                }
                safetyBrochuresDelivered = safetyBrochuresDelivered[0..^4];
            }
            
            dateOfArrival = DateTime.Now;

            StreamWriter writer = new StreamWriter("guestCheckIns\\" + name + " " + company + " " + dateOfArrival.ToString("dd - MM - yyyy") + ".txt"); 
            writer.WriteLine("Navn : " + name + "\nFirma : " + company + "\nDato for check-in : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "\nAnsvarlig medarbejder: " + responsiblePerson + "\nSikkerhedsbrochure er blevet afleveret: " + isSafetyBrochureDeliveredString + "\nSikkerhedsbrochurer som er blevet afleveret: " + safetyBrochuresDelivered);
            writer.Close();
            Console.Clear();
            Console.WriteLine("Gæsten er nu checket ind som:\n\nNavn : " + name + "\nFirma : " + company + "\nDato for check-in : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "\nAnsvarlig medarbejder: " + responsiblePerson + "\nSikkerhedsbrochure er blevet afleveret: " + isSafetyBrochureDeliveredString + "\nSikkerhedsbrochurer som er blevet afleveret: " + safetyBrochuresDelivered + "\n\nTryk Enter for at fortsætte");
            Console.ReadLine();
        }

        public void CheckOut()
        {
            Console.Clear();
            Console.WriteLine("Indtast venligst gæstens navn");
            name = Console.ReadLine();
            string[] listOfGuestCheckIns = Directory.GetFiles("guestCheckIns\\");


            foreach (var path in listOfGuestCheckIns)
            {
                StreamReader sr = new StreamReader(path);
                string guestCheckIn = sr.ReadLine();
                sr.Close();
                string[] guestCheckInSubStrings = guestCheckIn.Split(';');
                string checkInId = guestCheckInSubStrings[0];
                if (checkInId.Contains(name))
                {
                    File.Delete(path);
                    Console.WriteLine("Du er hermed checket ud. Tryk enter for at forsætte");
                    Console.ReadLine();
                    Console.Clear();
                }
            }

        }

        public void ShowCheckedIn()
        {
            string[] listOfGuests = Directory.GetFiles("guestCheckIns\\");
            Console.WriteLine("\nGæster\n");
            foreach (var path in listOfGuests)
            {
                StreamReader sr = new StreamReader(path);
                name = sr.ReadLine();
                company = sr.ReadLine();
                string dateOfArrivalString = sr.ReadLine();
                responsiblePerson = sr.ReadLine();
                string isSafetyBrochureDeliveredString = sr.ReadLine();
                safetyBrochuresDelivered = sr.ReadLine();
                sr.Close();
                Console.WriteLine(name + "\n" + company + "\n" + dateOfArrivalString + "\n" + responsiblePerson + "\n" + isSafetyBrochureDeliveredString + "\n" + safetyBrochuresDelivered + "\n");

            }
        }

        public static void RemoveOldCheckIns()
        {
            string[] bookedRooms = Directory.GetFiles("guestCheckIns\\");
            foreach (var path in bookedRooms)
            {
                StreamReader sr = new StreamReader(path);
                sr.ReadLine();
                sr.ReadLine();
                DateTime checkInTime = DateTime.Parse(sr.ReadLine().Remove(0, 20));
                sr.Close();
                int dateTimeComparison = DateTime.Compare(checkInTime, DateTime.Today);
                if (dateTimeComparison == -1)
                {
                    File.Delete(path);
                }
            }
        }
    }
}
