﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Hydac_Check_in_System___Team_2A
{
    public class SafetyBrochure
    {
        private string title;

        public string RetrieveSafetyBrochures()
        {
            string[] safetyBrochuresDelivered = new string[6];

            int j = 0;
            bool inputValid = false;
            while (inputValid != true)
            {
                
                try
                {
                    Console.Clear();
                    Console.WriteLine("Hvilken sikkerhedsbrochure vil du udlevere til gæsten?\n");
                    Console.WriteLine("0. Der behøves ikke afleveres en sikkerhedsbrochure");
                    int i = 1;

                    string[] safetyBrochures = Directory.GetFiles("SafetyBrochures\\");
                    foreach (var path in safetyBrochures)
                    {
                        string[] pathSubStrings = path.Split('\\', '.');
                        title = pathSubStrings[1];
                        Console.WriteLine(i + ". " + title);
                        i++;
                    }
                    Console.Write("\n");
                    int userInput = int.Parse(Console.ReadLine());
                    if (userInput <= safetyBrochures.Length && userInput > 0)
                    {
                        StreamReader sr = new StreamReader(safetyBrochures[userInput-1]);
                        Console.Clear();
                        Console.WriteLine(sr.ReadLine());
                        safetyBrochuresDelivered[j] = safetyBrochures[userInput-1];
                        inputValid = true;
                        j++;
                        Console.WriteLine("\n\nVil du udlevere flere sikkerhedsbrochure. Tast enten \"ja\" eller \"nej\"");
                        sr.Close();
                        bool input2Valid = false;
                        while (input2Valid != true)
                        {
                            string userInput2 = Console.ReadLine();
                            if (userInput2 == "ja")
                            {
                                input2Valid = true;
                                inputValid = false;
                            }
                            else if (userInput2 == "nej")
                            {
                                inputValid = true;
                                input2Valid = true;
                            }
                            else
                            {
                                Console.WriteLine("Ugyldigt Input. Prøv igen");
                            }
                        }

                    }
                    else if (userInput == 0)
                    {
                        safetyBrochuresDelivered[0] = "None delivered";
                        inputValid = true;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("\nUgyldigt input, prøv igen.\n");
                }
            }

            //Since we can't use Spplit later on an empty array element, we have to shorten the array. Lets start by counting how many elements in the array aren't null
            //We should probably make use of a collection or list, but since we're out of time, this will be the solution

            int elementsNotNull = 0;
            foreach (var element in safetyBrochuresDelivered)
            {
                if (element != null)
                {
                    elementsNotNull++;
                }
            }
            //Create a new array which has a length equal to the number of elements != null in the old array
            string[] safetyBrochuresDeliveredShortened = new string[elementsNotNull];
            //Transfer all elements from the old array to the new one
            for (int i = 0; i < safetyBrochuresDeliveredShortened.Length; i++)
            {
                safetyBrochuresDeliveredShortened[i] = safetyBrochuresDelivered[i];
            }

            string safetyBrochureDeliveredString = "";
            if (safetyBrochuresDeliveredShortened[0].Contains("None delivered"))
            {
                safetyBrochureDeliveredString = "None delivered";
            }
            else
            {
                foreach (var safetyBrochureDelivered in safetyBrochuresDeliveredShortened)
                {
                    try
                    {
                        string[] safetyBrochureDeliveredSubString = safetyBrochureDelivered.Split('\\', '.');
                        safetyBrochureDeliveredString += safetyBrochureDeliveredSubString[1] + ";";
                    }
                    catch (Exception)
                    {

                    }

                }
            }
            safetyBrochureDeliveredString = safetyBrochureDeliveredString.Trim(';');
            return safetyBrochureDeliveredString;
        }
    }
}
